![alt text](img/docker.png "Docker")

[Khaled Abdi - khaled@abdi.pro](mailto:khaled@abdi.pro)



<!-- .slide: data-background="img/comic.png" data-background-size="auto" -->



# Sommaire

 1. Présentation
 2. Installation et prise en main
 3. Gestion des ressources
 4. Images Docker
 5. L'orchestration



# Présentation

* Développé par [Solomon Hykes](https://fr.wikipedia.org/wiki/Solomon_Hykes) chez dotCloud en 2013
* Basé sur des fonctionnalités du noyau Linux
  * [LXC](https://linuxcontainers.org/lxc/), API bas niveau de conteneurisation
  * [cgroups](https://fr.wikipedia.org/wiki/Cgroups), limitation de l'accès aux ressources


## Virtualisation ?

![Docker vs VM](img/docker-vs-vm.jpg "Docker vs VM")<!-- .element style="width: 100%" -->


## Comparatif (1/3)

### Un léger overhead réseau
![network comparison](img/network-comparison.png "network comparison")<!-- .element style="height: 280px" -->
![network comparison](img/network-comparison2.png "network comparison")<!-- .element style="height: 280px" -->

[source](https://domino.research.ibm.com/library/cyberdig.nsf/papers/0929052195DD819C85257D2300681E7B/$File/rc25482.pdf)<!-- .element: class="source" -->


## Comparatif (2/3)

### Accès disques
![disk comparison](img/disk-comparison.png "disk comparison")<!-- .element style="height: 280px" -->

[source](https://domino.research.ibm.com/library/cyberdig.nsf/papers/0929052195DD819C85257D2300681E7B/$File/rc25482.pdf)<!-- .element: class="source" -->


## Comparatif (3/3)

### CPU
![cpu comparison](img/cpu-comparison.jpeg "cpu comparison")<!-- .element style="height: 280px" -->

[source](https://domino.research.ibm.com/library/cyberdig.nsf/papers/0929052195DD819C85257D2300681E7B/$File/rc25482.pdf)<!-- .element: class="source" -->


## Taille des images

* Poids de l'image Docker Debian : <strong>[23Mo](https://hub.docker.com/_/debian?tab=tags)</strong>
* Poids de l'image VirtualBox Debian : <strong>[1.2Go](https://www.osboxes.org/debian/)</strong>


## Cas d'usage

| Docker | Virtualisation |
|-|-|
|Linux|Linux/Windows/Android|
|Shell|Interface utilisateur|
|Microservice|Application complexe|


## Terminologie

| Docker | Virtualisation |
|-|-|
|Conteneur|VM|
|Image|Snapshot|
|Registry| ? |


## Architecture
![architecture](img/architecture.png "architecture")
[Source](https://www.aquasec.com/wiki/display/containers/Docker+Containers)<!-- .element: class="source" -->

## Ecosystème

* [Documentation Docker](https://docs.docker.com/)
* [Docker Hub](https://hub.docker.com/)<!-- .element class="fragment" -->



# Installation et prise en main
<!-- .slide: data-background="https://media.giphy.com/media/KpACNEh8jXK2Q/giphy.gif" data-background-size="100% auto" -->


## Linux

* [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [https://get.docker.com/](https://get.docker.com/)

>  Kernel > 3.10


### 1 - Installer docker-compose

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```


### 2 - Docker en mode utilisateur

```bash
sudo usermod -aG docker your-user
```
Puis on relance le shell


### 3 - Figer la version de Docker

```bash
sudo apt-mark hold docker-ce
```

* Kubernetes 1.13 supporte Docker **18.09** [source](https://kubernetes.io/docs/setup/release/notes/)
* Rancher 1.X supporte Docker **18.06** [source](https://rancher.com/docs/rancher/v1.6/en/hosts/)
* Rancher 2.X supporte Docker **18.09** [source](https://rancher.com/docs/rancher/v2.x/en/installation/requirements/)


## Mac

* https://docs.docker.com/docker-for-mac/install/

> MacOS 10.12+

(docker-compose est déjà installé)


## Windows

* https://docs.docker.com/docker-for-windows/install/

> Windows 10+ avec la virtualisation activée dans le BIOS

(docker-compose est déjà installé)


## Récupérer une image


### Registry -> Image

```bash
docker pull httpd
```

> Récupération sur [hub.docker.com](http://hub.docker.com)


### Registry privé -> Image

```bash
docker login registry.gitlab.com
docker pull registry.gitlab.com/docker/apache
```


### Quel tag choisir ?<!-- .element style="color: black" -->
<!-- .slide: data-background="img/docker-tags.png" data-background-size="100% auto" -->


### Tags

Permettent de choisir :
* la version
* parfois le type
  * php-fpm
  * php-apache
  * php-cli

> pas de tag = latest


### OS

* Ubuntu **32Mo**<!-- .element class="fragment" -->
* Debian **23Mo**<!-- .element class="fragment" -->
* Alpine **3Mo**<!-- .element class="fragment" -->


### Bonnes pratiques

* Préferez les images *Alpine*
* Evitez les images sans *Dockerfile*
* Préférez les images *officielles*


### Image -> Registry

```bash
docker login registry.gitlab.com
docker push registry.gitlab.com/docker/apache
```

> **registry.gitlab.com/docker/apache** est le nom de l'image


### Image -> Conteneur

```
docker run --name [container-name] httpd
```

> run = pull + create + start


### Conteneur -> Image

```
docker commit [container-name/id] myimage
```

> pas une bonne pratique


### États d'un conteneur

1. Up (Running)<!-- .element class="fragment" -->
2. Exited (Stopped)<!-- .element class="fragment" -->
3. Paused<!-- .element class="fragment" -->
4. Deleted<!-- .element class="fragment" -->
5. Created<!-- .element class="fragment" -->


<!-- .slide: data-background="img/lifecycle.png" data-background-size="100% auto" -->
[source](http://docker-saigon.github.io/post/Docker-Internals/)<!-- .element: class="source" -->


## Inventaire
Des images :
```
docker images
```

De tous les conteneurs :
```
docker ps -a
```

Des conteneurs up uniquement :
```
docker ps
```


## Quelques raccourcis


### 1 - Supprime le conteneur au stop

```bash
docker run --rm [image-name]
```


### 2 - Relance le conteneur si erreur ou redémarrage

```bash
docker run --restart=unless-stopped [image-name]
```

* always
* on-failure


### 3 - Stoppe et supprime le conteneur

```bash
docker rm --force [container-name/id]
```


### 4 - Stoppe tous les conteneurs up

```bash
docker stop $(docker ps -q)
```


### 5 - Supprime tous les conteneurs

```bash
docker rm $(docker ps -a -q)
```


### 6 - Supprime toutes les images

```bash
docker rmi $(docker images -q)
```


## Gestion des entrées/sorties

![tty](img/input-output.png "tty")<!-- .element style="width: 100%" -->


### Sorties connectées
```bash
docker run httpd
```

### Sorties détachées
```bash
docker run -d httpd
```

### Entrées / sorties connectées 
```bash
docker run -it httpd bash
```
> -it = --interactive -tty


## Intéragir avec un conteneur running


### Récupérer la sortie d'un conteneur
```bash
docker logs -f [container-name/id]
```


### Exécuter une commande dans un conteneur
```bash
docker exec [container-name/id] composer install
```


### Ouvrir un shell dans un conteneur
```bash
docker exec -it [container-name/id] [sh/bash]
```

```bash
docker exec -it $(docker ps -q) [sh/bash]
```
<!-- .element class="fragment" -->



<!-- .slide: data-background="https://media.giphy.com/media/yAOjunY81Trjy/giphy.gif" data-background-size="100% auto" -->
# Gestion des ressources


## Isolation

Un conteneur accède :
* Au CPU
* A la RAM
* Au réseau
* A un système de fichier

> Dans une stricte isolation des ressources de l'hôte.<!-- .element class="fragment" -->


## Limiter encore les ressources

### La mémoire
```bash
docker run --memory="512m" httpd
```

### Le CPU
```bash
docker run --cpus="1.5" httpd
```


## Cas du swap

Si limite de mémoire, le swap peut atteindre 2x cette limite.

Sinon limite de l'hôte.


## Le réseau

3 types de réseaux :
* bridge
* host
* none

```bash
docker network ls
```


### Bridge
![bridge](img/bridge1.png "bridge")<!-- .element style="height: 320px" -->


### Propriétés du réseau bridge

* Par défaut, un conteneur est ajouté au network **docker0**
* Il y a une ip fixe et un nom d'hôte
* Il accède au réseau mais ne peut être contacté en dehors de l'hôte


### Publication de ports 
![bridge](img/bridge2.png "bridge")<!-- .element style="height: 320px" -->
```bash
docker network create -d bridge mybridge
docker run --net mybridge --name db -d mysql 
docker run --net mybridge --name web -p 8000:5000 -d myapp
```


### Host
![host](img/host.png "host")<!-- .element style="height: 320px; background:white" -->
```bash
docker run --net host --name C1
docker run --net host --name nginx
```


### Propriétés du réseau host

* Les conteneurs partagent l'ip de l'hôte
* Chaque port exposé l'est sur l'hôte


### Réseau None

* Pas d'interface réseau virtuelle
* Conteneur complètement isolé


## Volumes

Espaces de stockage **persistents** et **partagés**.


### 3 types de volumes 
* le volume anonyme
```bash
docker run -v /path/in/container
```

* le volume nommé
```bash
docker volume create myvolume
docker run -v myvolume:/path/in/container
```

* le volume hôte
```bash
docker run -v /path/on/host:/path/in/container
```


### Caractéristiques des volumes

* montés au run
* avec un cycle de vie différent
* initialisé avec le contenu du volume pas celui du conteneur


### Les drivers
* [Glusterfs](https://www.gluster.org/)
* [Portworx](https://portworx.com/)
* [vShpere](https://vmware.github.io/vsphere-storage-for-docker/)
* Azure, Amazon S3, Google Cloud ...

[source](https://docs.docker.com/engine/extend/legacy_plugins/)<!-- .element: class="source" -->


## Travaux pratiques

Avec Docker :
* créer une commande qui s'exécute comme npm
* créer une commande qui sert le répertoire courant dans Apache



<!-- .slide: data-background="https://media.giphy.com/media/xTka00p9HHoaLgS2Jy/giphy.gif" data-background-size="100% auto" -->
# Images Docker


![architecture](img/architecture.png "architecture")


## Construire une image
[FROM scratch](https://hub.docker.com/_/scratch)

Ou

[FROM alpine](https://pkgs.alpinelinux.org/packages)


## Construction par couches

![layers](img/layers.png)<!-- .element style="height: 320px;" -->
```bash
docker history php:7.3-apache
```


## Ecriture du Dockerfile

```Dockerfile
FROM php:7.3-apache
COPY www/index.php /var/www/html
```

> Copiez dans un fichier *Dockerfile* dans un répertoire vide.


## Build de l'image
```bash
docker build -t myapp .
```


## Lancement d'un conteneur
```bash
docker run --rm -p 80:80 myapp
```


## Gestion du cache (1/2)

Chaque couche est mise en cache.

Le cache d'une commande est invalidé quand :
1. la commande change<!-- .element class="fragment" -->
2. un des fichiers ciblé par un COPY change<!-- .element class="fragment" -->


## Gestion du cache (2/2)

Si le cache d'une commande est invalidé, **toutes les commandes suivantes** sont relancées.


## Bonnes pratiques


### 1 - Grouper les commandes

```Dockerfile
RUN wget http://.../file.zip
RUN unzip file.zip
RUN rm file.zip
```
vs
```Dockerfile
RUN wget http://.../file.zip
 && unzip file.zip
 && rm file.zip
```

Une couche sans le poids du fichier zip.


### 2 - Ordonner les commandes

```Dockerfile
COPY www/ /var/www/html
RUN apt-get install zip
```
vs
```Dockerfile
RUN apt-get install zip
COPY www/ /var/www/html
```

Si un fichier est changé dans www/, l'installation de zip ne se refait pas.


### 3 - Créer un .dockerignore

```.dockerignore
.git
~*
```

Au build, le répertoire **www/** est entièrement scanné.


### 4 - Utiliser des tags précis

```Dockerfile
FROM php
```
vs
```Dockerfile
FROM php:7.3
```

Dans quelques années, la première image pourrait se baser sur php8...


![Alpine](img/alpinelinux.svg "Alpine")<!-- .element style="background: white" -->


## Quelques arguments ...

* Images réduites par rapport à Debian ou Ubuntu
* Reste relativement simple ...


## Installation de packages
```Dockerfile
RUN apt-get update && apt-get install -y \
		ca-certificates \
		curl \
		xz-utils \
--no-install-recommends && rm -r /var/lib/apt/lists/*
```
vs
```Dockerfile
RUN apk add --no-cache \
		ca-certificates \
		curl \
		tar \
		xz
```
https://pkgs.alpinelinux.org/packages


## Installation temporaire
```Dockerfile
RUN apk add --no-cache --virtual .fetch-deps \
    gnupg \
    wget

...

    apk del --no-network .fetch-deps
```


## Focus sur quelques commandes

### COPY
```Dockerfile
COPY --chown www-data:www-data www/ /var/www/html
COPY docker-entrypoint.sh /usr/local/bin
```


### ENV
```Dockerfile
ENV DOCUMENT_ROOT=/var/www/html \
  FPM_PM=ondemand \
  FPM_PM_MAX_CHILDREN=5 \
  FPM_PM_START_SERVERS=2 \
```

**Valeurs par défaut** de variables d'environnement.


### EXPOSE
```Dockerfile
EXPOSE 8080
```
Bonne pratique mais utile en network **host** uniquement.


### WORKDIR
```Dockerfile
WORKDIR /app
```
Chemin de référence pour tous les autres commandes (RUN, COPY, ENTRYPOINT, CMD)


### ENTRYPOINT et CMD
```Dockerfile
ENTRYPOINT ["script"]
CMD ["p1"]
```

Quelques exemples :
```bash
docker run myimage -> "script p1"
docker run myimage p2 -> "script p2"
docker run --entrypoint='' myimage p2 -> "p2"
```

Le script entrypoint se termine souvent par :
```bash
/bin/bash -c "$@"
exec "$@"
```


## Cas pratique

```Dockerfile
FROM php:5.6-fpm
COPY www /usr/src/sources
# Copy new entrypoint that creates new user
COPY docker-entrypoint.sh /sbin/docker-entrypoint-override.sh
# Install necessary PHP extensions
RUN docker-php-ext-install \
	mysqli
# Change entrypoint
ENTRYPOINT ["/sbin/docker-entrypoint-override.sh"]
CMD ["php-fpm"]
```


## Améliorations ?


## Solutions
1. Descendre le COPY des sources<!-- .element class="fragment" -->
1. Utiliser l'image php-apache<!-- .element class="fragment" -->
2. Ou se construire une image php-fpm avec un 2nd process apache<!-- .element class="fragment" -->


## Problème central
Le déploiement *Docker* est fait pour les *microservices* :
* stateless
* scalables


## Solution générale

# [Les 12 facteurs](https://12factor.net/fr/)


## Quelques implications 
1. La base de données est un service externe (comme l'envoi d'email)<!-- .element class="fragment" -->
2. La configuration (différente d'un déploiement à l'autre) se fait via variables d'environnement<!-- .element class="fragment" -->
3. Un conteneur doit se lancer rapidement et s'arrêter proprement<!-- .element class="fragment" -->
4. La gestion des dépendances ne se fait pas à l'exécution<!-- .element class="fragment" -->



<!-- .slide: data-background="https://media.giphy.com/media/xAu73NvBCNDZC/giphy.gif" data-background-size="100% auto" -->
# L'orchestration


## Objectif

Assurer le déploiement rapide et stable des applications.


## Avant de commencer

1. Même version de Docker sur tous les serveurs
2. Désactivation du swap intempestif

```bash
sudo sysctl -a |grep 'vm.swapp*\|vm.over*'
```

```bash
sudo sysctl vm.swappiness=0
sudo sysctl vm.overcommit_memory=1
```


## Monitorer ses serveurs


### Dans le shell
```bash
docker stats
```


### Avec CAdvisor
![cadvisor](img/cadvisor.png "cadvisor")

[Website](https://github.com/google/cadvisor)


### Installation (CAdvisor + Node-exporter)
```yaml
version: '2'
services:
  node-exporter:
    image: prom/node-exporter:latest
    volumes:
    - /proc:/host/proc:ro
    - /sys:/host/sys:ro
    - /:/rootfs:ro
    ports:
    - 9100:9100
    command:
    - --path.procfs=/host/proc
    - --path.sysfs=/host/sys
    - --collector.filesystem.ignored-mount-points
    - ^/rootfs/(mnt|sys|proc|dev|host|etc|var/lib/docker/containers|var/lib/docker/overlay2|run/docker/netns|var/lib/docker/aufs)($$|/)
  cadvisor:
    image: google/cadvisor:latest
    volumes:
    - /var/run:/var/run:rw
    - /:/rootfs:ro
    - /sys:/sys:ro
    - /var/lib/docker/:/var/lib/docker:ro
    ports:
    - 9101:8080
```


### Avec Prometheus+Grafana
![Prometheus](img/prometheus-grafana.jpg "Prometheus")

[Prometheus](https://prometheus.io/) - [Grafana](https://grafana.com/)


### Installation
```yaml
version: '2'
volumes:
  grafana-data: {}
  prometheus-data: {}
services:
  grafana:
    image: grafana/grafana:latest
    volumes:
    - /mnt/infra/grafana/config:/etc/grafana
    - /mnt/infra/grafana/data:/var/lib/grafana
    expose:
    - '3000'
    user: root
  prometheus:
    image: prom/prometheus:latest
    volumes:
    - /mnt/infra/prometheus:/etc/prometheus/
    - prometheus-data:/prometheus
    expose:
    - '9090'
    command:
    - --config.file=/etc/prometheus/prometheus.yml
    - --storage.tsdb.path=/prometheus
    - --web.console.libraries=/etc/prometheus/console_libraries
    - --web.console.templates=/etc/prometheus/consoles
    - --storage.tsdb.retention=200h
```


## Le nettoyeur

* Supprime les conteneurs arrêtés
* Supprime les images orphelines
* Supprime les volumes orphelins

[meltwater/docker-cleanup](https://github.com/meltwater/docker-cleanup)


### Installation
```yaml
version: '2'
services:
  cleanup:
    privileged: true
    image: meltwater/docker-cleanup:1.8.0
    environment:
      CLEAN_PERIOD: '3600'
      DEBUG: '0'
      DELAY_TIME: '900'
      KEEP_CONTAINERS: '*:*'
      KEEP_CONTAINERS_NAMED: '*-datavolume'
      KEEP_IMAGES: rancher/
      LOOP: 'true'
    network_mode: none
    volumes:
    - /var/run/docker.sock:/var/run/docker.sock
    - /var/lib/docker:/var/lib/docker
```


## Docker-compose

Outil permettant de déclarer dans un fichier yml :
* les conteneurs à lancer avec leurs paramètres :
  * image
  * port
  * volumes
  * ...
* le(s) réseau(x)
* le(s) volume(s)


## Exemple
```yml
version: '3'
services:
  web:
    image: registry.gitlab.com/myapp/image:1.1
    ports:
    - "8000:80"
    volumes:
    - .:/code
    restart: "unless-stopped"
  mysql:
    image: mysql
    volumes:
    - mysql:/var/lib/mysql
volumes:
  mysql: {}
```


## Les commandes

Lancer l'application :
```
docker-compose up [-d]
```

Stopper l'application :
```
docker-compose stop
```


## Astuces

Le fichier **docker-compose.override.yml** permet de surcharger les paramètres.

Non versionné, il permet de gérer la config dépendante à chaque environnement.


## Avantages / Inconvénients

* Simple<!-- .element: class="positive" -->
* Solution ISO dev / preprod / prod<!-- .element: class="positive" -->
* Déploiement manuel<!-- .element: class="negative" -->
* Peu scalable<!-- .element: class="negative" --> 


## Problème du mapping de ports

2 applications ne peuvent pas écouter sur le port 80 du même hôte


<!-- .slide: data-background-color="white" data-background="img/traefik-architecture.svg" data-background-size="100% auto" -->


## Traefik
Solution de Load balancing
* dynamique (auto découverte via des labels)
* indépendant de l'orchestrateur (docker-compose, Rancher, Kubernetes ...)
* complet (https, http2 ...) et performant

[Traefik.io](https://traefik.io/)


## Rancher
![Rancher](img/rancher.png "Rancher")


### Version 1.6 à l'arrêt depuis 2018
* basée sur Docker-compose
* stable
* Cattle : orchestrateur petit mais costaud


### Hôtes
S'ajoutent facilement depuis :
* Custom
* Amazon EC2
* Azure 


### Orchestration par étiquettes :
* sur les conteneurs :
![Rancher](img/rancher1.png "Rancher")

* sur les hôtes :

![Rancher](img/rancher2.png "Rancher")


### Quelques étiquettes spécifiques :
Forcer le conteneur à être sur le même hôte qu'apache2 (compagnon)
* io.rancher.sidekicks=apache2  

Forcer le conteneur à être déployé sur tous les hôtes
* io.rancher.scheduler.global=true


### Bilan de santé

Commun à toutes les solutions d'orchestration.
> Permet de relancer automatiquement un container planté


<!-- .slide: data-background-color="white" data-background="img/healthcheck.png" data-background-size="100% auto" -->


### Rancher 1.6
* Interface & concepts simples<!-- .element: class="positive" -->
* Orchestration automatisée<!-- .element: class="positive" -->
* Stabilité et performances moyennes<!-- .element: class="negative" -->
* Abandonné<!-- .element: class="negative" -->


## Rancher 2


### Des différences de vocabulaire
|Rancher 1.6|Rancher 2|
|-|-|
|Service|Pod|
|Stack|Workload / Service|
|Host|Node|
|Environment|Cluster|


### Basé sur Kubernetes
Rancher devient une interface pour Kubernetes.

Mais aussi un moyen simple de le déployer!

```bash
docker run -d --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher
```


### C'était juste un début ...
On a simplement l'interface, aucun cluster!


### Cluster Kubernetes
![Kubernetes](img/kubernetes.svg "Kubernetes")<!-- .element: style="background: white;" -->


### Les besoins
|Composant|Description|Qt min|
|-|-|-|
|Etcd|Base de données de l'état des clusters|3|
|Control plane|Applicatifs Kubernetes : Manager, Scheduler...|2|
|Worker|Serveurs où s'exécutent les containers|2| 

[source](https://rancher.com/docs/rancher/v2.x/en/cluster-provisioning/)<!-- .element: class="source" -->


### Sur le Worker

Ne tourne que le *kubelet* :
* Monitore les ressources disponibles
* Vérifie l'état de santé des containers

>  plus léger que l'approche Rancher 1.X


### Ingress

Load Balancer de Kubernetes

[source](https://kubernetes.io/docs/concepts/services-networking/ingress/)<!-- .element: class="source" -->


## Rancher 2
* Stable depuis 2018<!-- .element: class="positive" -->
* Basé sur Kubernetes (bye bye cattle)<!-- .element: class="positive" -->
* Interface "simple"<!-- .element: class="positive" -->
* Support de docker-compose abandonné<!-- .element: class="negative" -->


<!-- .slide: data-background="img/rocket.png" data-background-size="100% auto" -->
# Merci