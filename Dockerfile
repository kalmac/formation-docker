FROM nbrown/revealjs:alpine

COPY index.html /reveal.js/
COPY content.md /reveal.js/
COPY css/custom.css /reveal.js/css/custom.css
COPY img/ /reveal.js/img/
